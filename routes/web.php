<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TableController@index');
Route::get('/departments', 'DepartmentsController@show');
Route::get('/departments/create', 'DepartmentsController@create');
Route::get('/departments/{id}/delete','DepartmentsController@delete');
Route::get('/departments/{id}/update','DepartmentsController@showDepartmentById');

Route::get('/employees', 'EmployeesController@show');
Route::get('/employees/create', 'EmployeesController@create');
Route::get('/employees/create', 'DepartmentsController@showAllDepartments');
Route::get('employees/{id}/delete','EmployeesController@delete')->name('deleteEmployee');
Route::get('employees/{id}/update','EmployeesController@showEmployeeById')->name('updateEmployee');

Route::post('/employees','EmployeesController@store');
Route::post('/employees/{id}/update','EmployeesController@update');
Route::post('/departments','DepartmentsController@store');
Route::post('/departments/{id}/update','DepartmentsController@update');


