<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Department
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Employee[] $employees
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department query()
 */
	class Department extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\Employee
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Department[] $departments
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Employee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Employee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Employee query()
 */
	class Employee extends \Eloquent {}
}

namespace App{
/**
 * App\Department_employee
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department_employee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department_employee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department_employee query()
 */
	class Department_employee extends \Eloquent {}
}

