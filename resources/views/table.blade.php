@extends('layouts.master')
@section('content')
    <div class="row mt-9">
        @if (count($stuff) > 0 && count($allDepartments) > 0)
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th></th>
                    @foreach($allDepartments as $department)
                    <th>{{ $department->department_name }}</th>
                    @endforeach
                </tr>
                </thead>
                @foreach($stuff as $item)
                <tbody>
                <tr>
                    <td>{{ $item->first_name }} {{ $item->last_name }}</td>
                    @foreach($allDepartments as $department)
                        <td> {{ $item->departments->contains($department) ? '+' : '' }}</td>
                    @endforeach
                </tr>
                </tbody>
                @endforeach
            </table>
        @else
            <p>Здесь пока нет записей. Чтобы увидеть таблицу добавьте хотя бы одного <a href="{{url('/employees')}}"> сотрудника </a> и <a
                        href="{{url('/departments')}}"> отдел</a>.</p>
        @endif
    </div>
@endsection
