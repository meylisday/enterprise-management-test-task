@extends('layouts.master')
@section('content')
    <div class="row mt-9">
        <div class="col-lg-6">
            <div class="container">
                <h1>Редактирование информации о сотруднике</h1>
                @foreach($data as $val)
                    @include ('layouts.errors')
                    <form method="POST" action="/employees/{{$val->id}}/update">
                        @csrf
                        <fieldset class="form-fieldset">
                            <div class="form-group">
                                <label class="form-label">Имя<span class="form-required">*</span></label>
                                <input type="text" class="form-control" name="first_name" value="{{ $val->first_name }}">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Фамилия<span class="form-required">*</span></label>
                                <input type="text" class="form-control" name="last_name" value="{{ $val->last_name }}">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Отчество</label>
                                <input type="text" class="form-control" name="middle_name" value="{{ $val->middle_name }}">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Пол</label>
                                <select class="form-control custom-select" name="gender">
                                    <option value="male" {{ ($val->gender == "male") ? 'selected' : '' }}>Мужской</option>
                                    <option value="female" {{ ($val->gender == "female") ? 'selected' : ''}}>Женский</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Заработная плата</label>
                                <input type="number" class="form-control" name="salary" min="1" value="{{ $val->salary }}">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Отделы<span class="form-required">*</span></label>
                                <select class="form-control custom-select" name="departments[]" multiple="multiple">
                                    @foreach ($allDepartments as $dep)
                                        <option value="{{ $dep->id }}" {{ $val->departments->contains($dep) ? 'selected' : '' }}>{{ $dep->department_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </fieldset>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </form>
                @endforeach
            </div>
        </div>
    </div>
@endsection

