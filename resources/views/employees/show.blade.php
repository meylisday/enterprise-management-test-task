@extends('layouts.master')
@section('content')
    <div class="row mt-6">
        <div class="col-lg-12">
            <div class="card-body text-right card-body--padding">
                <a href="{{url('employees/create')}}" class="btn btn-primary" role="button">Добавить</a>
            </div>
            @if (count($employees) > 0)
            <div class="card">
                <div class="table-responsive">
                    <table class="table card-table table-striped table-vcenter">
                        <thead>
                        <tr>
                            <th>Имя</th>
                            <th>Фамилия</th>
                            <th>Отчество</th>
                            <th>Пол</th>
                            <th>Заработная плата</th>
                            <th>Отделы</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($employees as $employee)
                            <tr>
                                <td>{{ $employee->first_name }}</td>
                                <td>{{ $employee->last_name }}</td>
                                <td>{{ $employee->middle_name }}</td>
                                <td>{{ ($employee->gender == 'female') ? 'Женский' : 'Мужской' }} </td>
                                <th>{{ $employee->salary }}</th>
                                <th>{{ $employee->departments->pluck('department_name')->implode(', ') }}</th>
                                <td class="w-1"><a href="{{ route('updateEmployee', ['id' => $employee->id]) }}" class="icon"><i class="fas fa-pen"></i></a></td>
                                <td class="w-1"><a href="{{ route('deleteEmployee', ['id' => $employee->id]) }}" class="icon"><i class="far fa-trash-alt"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @else
                <p>Добавьте сотрудников, чтобы увидеть таблицу.</p>
            @endif
        </div>
    </div>
@endsection
