@extends('layouts.master')
@section('content')
    <div class="row mt-9">
        <div class="col-lg-6">
            <div class="container">
                <h1>Добавление нового сотрудника</h1>
                @include ('layouts.errors')
                <form method="POST" action="/employees">
                    @csrf
                    <fieldset class="form-fieldset">
                        <div class="form-group">
                            <label class="form-label">Имя<span class="form-required">*</span></label>
                            <input type="text" class="form-control" name="first_name">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Фамилия<span class="form-required">*</span></label>
                            <input type="text" class="form-control" name="last_name">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Отчество</label>
                            <input type="text" class="form-control" name="middle_name">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Пол</label>
                            <select class="form-control custom-select" name="gender">
                                <option value="female">Женский</option>
                                <option value="male">Мужской</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Заработная плата</label>
                            <input type="number" class="form-control" name="salary" min="1">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Отделы<span class="form-required">*</span></label>
                            <select class="form-control custom-select" name="departments[]" multiple="multiple">
                                @foreach($departments as $department)
                                    <option value="{{ $department->id }}">{{ $department->department_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </fieldset>
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </form>
            </div>
        </div>
    </div>
@endsection