<div class="header py-4">
    <div class="container">
        <div class="d-flex">
            <a class="header-brand" href="{{url('/')}}">
                Enterprise Management
            </a>
            <div class="d-flex order-lg-2 ml-auto">
                <div class="nav-item d-none d-md-flex">
                    <a href="{{url('employees')}}" class="nav-link">Сотрудники</a>
                </div>
                <div class="nav-item d-none d-md-flex">
                    <a href="{{url('departments')}}" class="nav-link">Отделы</a>
                </div>
            </div>
        </div>
    </div>
</div>