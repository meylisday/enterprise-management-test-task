@extends('layouts.master')
@section('content')
    <div class="row mt-9">
        <div class="col-lg-6">
            <div class="container">
                <h1>Редактирование отдела</h1>
                @foreach($data as $val)
                    @include ('layouts.errors')
                    <form method="POST" action="/departments/{{$val->id}}/update">
                        @csrf
                        <fieldset class="form-fieldset">
                            <div class="form-group">
                                <label class="form-label">Название отдела<span class="form-required">*</span></label>
                                <input type="text" class="form-control" name="department_name" value="{{$val->department_name}}">
                            </div>
                        </fieldset>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </form>
                @endforeach
            </div>
        </div>
    </div>
@endsection