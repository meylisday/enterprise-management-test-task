@extends('layouts.master')
@section('content')
    <div class="row mt-6">
        <div class="col-lg-12">
            <div class="card-body text-right card-body--padding">
                <a href="{{url('departments/create')}}" class="btn btn-primary" role="button">Добавить</a>
            </div>
            @include('layouts.errors')
            @if (count($departments) > 0 )
            <div class="card">
                <div class="table-responsive">
                    <table class="table card-table table-striped table-vcenter">
                        <thead>
                        <tr>
                            <th>Название отдела</th>
                            <th>Количество сотрудников</th>
                            <th>Максимальная заработная плата</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($departments as $department)
                        <tr>
                            <td>{{$department->department_name}}</td>
                            <td>{{ $department->employees->count() }}</td>
                            <td>{{ $department->employees->max('salary') }}</td>
                            <td class="w-1"><a href="/departments/{{ $department->id }}/update" class="icon"><i class="fas fa-pen"></i></a></td>
                            <td class="w-1"><a href="/departments/{{ $department->id }}/delete" class="icon"><i class="far fa-trash-alt"></i></a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
                @else
            <p> Добавьте хотя бы один отдел, чтобы увидеть таблицу.</p>
                @endif
        </div>
    </div>
@endsection