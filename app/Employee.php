<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{

    protected $fillable = ['first_name', 'last_name', 'middle_name', 'gender', 'salary'];

    public function departments()
    {
        return $this->belongsToMany('App\Department', 'department_employees');
    }
}
