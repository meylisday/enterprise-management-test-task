<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department_employee extends Model
{
    protected $fillable = ['department_id', 'employee_id'];
}
