<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;

class DepartmentsController extends Controller
{
    public function show()
    {
        $departments = Department::with('employees')->orderBy('created_at', 'desc')->get();
        return view('departments.show', compact('departments'));
    }

    public function create()
    {
        return view('departments.create');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'department_name' => 'required|unique:departments',
        ]);
        $department = new Department();
        $department->department_name = $request->department_name;
        $department->save();
        return redirect('/departments');
    }

    public function delete($id)
    {
        $department = Department::find($id);
        try {
            $department->delete($department->id);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['Нельзя удалить отдел, пока в нем есть хотя бы один сотрудник!']);
        }
        return redirect('/departments');
    }

    public function showDepartmentById($id){
        $data = Department::where('id', $id)->get();
        return view('departments.update', compact('data'));
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $department = Department::find($id);
        $department->department_name = $request->department_name;
        $department->save();
        return redirect('/departments');
    }

    public function showAllDepartments()
    {
        $departments = Department::all();
        return view('employees.create', compact('departments'));
    }
}
