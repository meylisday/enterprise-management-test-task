<?php

namespace App\Http\Controllers;

use App\Department;
use App\Employee;
use App\Http\Requests\PostRequest;
use Illuminate\Http\Request;
use Validator;

class EmployeesController extends Controller
{
    public function show()
    {
      $employees = Employee::with('departments')->get();
      return view('employees.show', compact('employees'));
    }

    public function create()
    {
        return view('employees.create');
    }

    public function store(PostRequest $request)
    {
        $employee = Employee::create(request(['first_name', 'last_name', 'middle_name', 'gender', 'salary']));
        $employee = Employee::find($employee->id);
        $employee->departments()->attach($request->departments);
        return redirect('/employees');
    }

    public function delete($id)
    {
        $employee = Employee::find($id);
        $employee->delete($employee->id);
        return redirect('/employees');
    }

    public function showEmployeeById($id)
    {
        $allDepartments = Department::all();
        $data = Employee::with('departments')->where('id', $id)->get();
        return view('employees.update', compact('data', 'allDepartments'));
    }

    public function update(PostRequest $request)
    {
        $id = $request->id;
        $employee = Employee::find($id);
        $employee->update(request(['first_name', 'last_name', 'middle_name', 'gender', 'salary']));
        $employee = Employee::findOrFail($employee->id);
        $employee->departments()->sync($request->departments, true);
        return redirect('/employees');
    }

}
