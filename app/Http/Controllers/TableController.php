<?php

namespace App\Http\Controllers;

use App\Department;
use App\Employee;

class TableController extends Controller
{
    public function index()
    {
        $stuff = Employee::with('departments')->get();
        $allDepartments = Department::all();
        return view('table', compact('stuff', 'allDepartments'));
    }
}
