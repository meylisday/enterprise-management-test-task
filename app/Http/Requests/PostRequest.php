<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $arr = explode('@', $this->route()->getActionName());
        $method = $arr[1];
        switch ($method) {
            case 'store':
                return [
                    'first_name' => 'alpha|required|unique_with:employees,last_name, middle_name',
                    'last_name' => 'required|alpha',
                    'middle_name' => 'nullable|alpha',
                    'salary' => 'nullable|integer|between:1,1000000',
                    'departments' => 'required'
                ];
                break;
            case 'update':
                return [
                    'first_name' => 'required|alpha',
                    'last_name' => 'required|alpha',
                    'middle_name' => 'nullable|alpha',
                    'salary' => 'nullable|integer|between:1,1000000',
                    'departments' => 'required'
                ];
                break;
        }
    }

    public function messages()
    {
        return [
            'first_name.alpha' => 'Имя может содержать только буквы',
            'last_name.alpha' => 'Фамилия может содержать только буквы',
            'middle_name.alpha' => 'Отчество может содержать только буквы',
            'departments.required' => 'Сотрудник должен иметь хотя бы один отдел.',
            'salary.integer' => 'Вводимое значение заработной платы должно быть числом',
            'salary.between' => 'Вводимое значение не должно привышать 1000000',
            'first_name.required' => 'Имя обязательно для заполнения',
            'last_name.required' => 'Фамилия обязательна для заполнения',
            'first_name.unique_with' => 'Сотрудник с таким ФИО уже существует'
        ];
    }
}
